import _ from 'lodash';
import butterchurn from 'butterchurn';
import $ from 'jquery';

import './lib/butterchurnPresets.min.js';
import './lib/butterchurnPresetsExtra.min.js';

const { butterchurnPresets, butterchurnPresetsExtra } = window;

var AudioContext =
  window.AudioContext || // Default
  window.webkitAudioContext || // Safari and old versions of Chrome
  false;

var isPlaying = false;
var visualizer = null;
var rendering = false;
var audioContext = null;
var sourceNode = null;
var delayedAudible = null;
var cycleInterval = null;
var presets = {};
var presetKeys = [];
var presetIndexHist = [];
var presetIndex = 0;
var presetCycle = false;
var presetCycleLength = 15000;
var presetRandom = false;
var canvas = document.getElementById('canvas');

var reverb = null;
var gainNode = null;
var distortion = null;
var biquadFilter = null;

var mouseX,
  mouseY = 0;

var currentPreset;
var idlePreset = 0;
var isIdle;

var context;
var bufferLoader;

var mainBuffer;
var notificationBuffer;
var idleBuffer;

// var baseUrl = "http://vps242298.ovh.net:3333"
// var baseUrl = "https://localhost:3333"
var baseUrl = 'https://vizzz.club';

function resizeCanvas() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  visualizer = butterchurn.createVisualizer(audioContext, canvas, {
    width: canvas.width,
    height: canvas.height,
    pixelRatio: window.devicePixelRatio || 1,
    textureRatio: 1,
  });
  if (presetKeys != undefined) {
    if (isIdle) {
      visualizer.loadPreset(presets[presetKeys[idlePreset]], 0);
    } else {
      if (currentPreset) {
        visualizer.loadPreset(presets[presetKeys[currentPreset]], 0);
      }
    }
  }
}

function connectToAudioAnalyzer(sourceNode, muted) {
  if (delayedAudible) {
    delayedAudible.disconnect();
  }

  if (!audioContext) {
    return;
  }

  delayedAudible = audioContext.createDelay();
  delayedAudible.delayTime.value = 0.26;

  // reverb = audioContext.createConvolver();

  gainNode = audioContext.createGain();
  gainNode.gain.value = 5.0;
  if (muted) {
    gainNode.gain.value = 0.0;
  }

  biquadFilter = audioContext.createBiquadFilter();
  biquadFilter.type = 'bandpass';
  biquadFilter.frequency.setValueAtTime(mouseY * 10, audioContext.currentTime);
  biquadFilter.Q.setValueAtTime(5, audioContext.currentTime);

  sourceNode.connect(delayedAudible);
  // delayedAudible.connect(audioContext.destination);

  // delayedAudible.connect(distortion);
  // distortion.connect(biquadFilter)
  delayedAudible.connect(biquadFilter);
  biquadFilter.connect(gainNode);
  gainNode.connect(audioContext.destination);

  visualizer.connectAudio(delayedAudible);
}

function startRenderer() {
  requestAnimationFrame(() => startRenderer());
  visualizer.render();
}

function playBufferSource(_buffer) {
  if (!rendering) {
    rendering = true;
    startRenderer();
  }

  if (sourceNode) {
    sourceNode.disconnect();
  }

  sourceNode = audioContext.createBufferSource();
  sourceNode.buffer = _buffer;

  var muted;
  if (_buffer == idleBuffer) {
    muted = true;
  } else {
    muted = false;
  }
  connectToAudioAnalyzer(sourceNode, muted);

  sourceNode.loop = true;
  sourceNode.start(0);

  updateEffects();
}

var voiceTimeout;

function stopVoice() {
  if (!sourceNode) return;
  sourceNode.stop();
  clearTimeout(voiceTimeout);
}

function startVoice() {
  clearTimeout(voiceTimeout);
  playBufferSource(mainBuffer);
}

function loadAudio(url, callback) {
  context = new AudioContext();
  fetch(url)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => context.decodeAudioData(arrayBuffer))
    .then(audioBuffer => {
      // _buffer = audioBuffer;
      callback(audioBuffer);
    });
}

loadAudio(baseUrl + '/mp3', function(buffer) {
  mainBuffer = buffer;
});
loadAudio('assets/audio/notification_2b.wav', function(buffer) {
  notificationBuffer = buffer;
});
loadAudio('assets/audio/viz.mp3', function(buffer) {
  idleBuffer = buffer;
});

initPlayer();
loadData();
resizeCanvas();

function updateEffects() {
  if (sourceNode) {
    if (mouseX) {
      sourceNode.playbackRate.value = 0.0 + (mouseX / canvas.width) * 2;
    } else {
      sourceNode.playbackRate.value = 1.0;
    }
  }
  if (biquadFilter) {
    biquadFilter.frequency.setValueAtTime(
      mouseY * 10,
      audioContext.currentTime,
    );
  }
}

function initPlayer() {
  audioContext = new AudioContext();

  presets = {};
  if (butterchurnPresets) {
    Object.assign(presets, butterchurnPresets.getPresets());
  }
  if (butterchurnPresetsExtra) {
    Object.assign(presets, butterchurnPresetsExtra.getPresets());
  }
  presets = _(presets)
    .toPairs()
    .sortBy(([k, v]) => k.toLowerCase())
    .fromPairs()
    .value();
  presetKeys = _.keys(presets);
  presetIndex = Math.floor(Math.random() * presetKeys.length);

  visualizer = butterchurn.createVisualizer(audioContext, canvas, {
    width: canvas.width,
    height: canvas.height,
    pixelRatio: window.devicePixelRatio || 1,
    textureRatio: 1,
  });
  // nextPreset(0);
  // cycleInterval = setInterval(() => nextPreset(2.7), presetCycleLength);

  var presetName = presetKeys[1];
  visualizer.loadPreset(presets[presetName], 0);
  // visualizer.loadPreset(shader(), 0);
  // console.log( shader());
  // console.log(presets[presetName]);
}

function loadData() {
  var url = baseUrl;
  httpRequest(baseUrl + '/status');

  function httpRequest(url = '') {
    fetch(url, {
      // headers: {
      //   "Content-Type": "application/json"
      // },
    })
      .then(function(res) {
        return res.json();
      })
      .then(function(data) {
        console.log(data);

        var presetName = presetKeys[data.preset];
        currentPreset = data.preset;
        visualizer.loadPreset(presets[presetName], 0);
      });
  }
}

$('body').mousemove(function(event) {
  mouseX = event.offsetX;
  mouseY = event.offsetY;

  updateEffects();
});

window.addEventListener('resize', resizeCanvas, false);

$(document).keydown(e => {
  if (e.key === ' ' || e.key === 'ArrowRight') {
    // nextPreset();
  } else if (e.key === 'Backspace' || e.key === 'ArrowLeft') {
    // prevPreset();
  } else if (e.key === 'h') {
    // nextPreset(0);
  } else if (e.key === 'o') {
    goIdle();
  } else if (e.key === 'p') {
    goPlaying();
  }
});

function BufferLoader(context, urlList, callback) {
  this.context = context;
  this.urlList = urlList;
  this.onload = callback;
  this.bufferList = new Array();
  this.loadCount = 0;
}

BufferLoader.prototype.loadBuffer = function(url, index) {
  // Load buffer asynchronously
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.responseType = 'arraybuffer';

  var loader = this;

  request.onload = function() {
    // Asynchronously decode the audio file data in request.response
    loader.context.decodeAudioData(
      request.response,
      function(_buffer) {
        if (!_buffer) {
          alert('error decoding file data: ' + url);
          return;
        }
        loader.bufferList[index] = _buffer;
        if (++loader.loadCount == loader.urlList.length)
          loader.onload(loader.bufferList);
      },
      function(error) {
        console.error('decodeAudioData error', error);
      },
    );
  };

  request.onerror = function() {
    alert('BufferLoader: XHR error');
  };

  request.send();
};

BufferLoader.prototype.load = function() {
  for (var i = 0; i < this.urlList.length; ++i)
    this.loadBuffer(this.urlList[i], i);
};

/// PLAY AND STOP FUNCTIONS

function goIdle() {
  clearTimeout(voiceTimeout);
  playBufferSource(idleBuffer);
  visualizer.loadPreset(presets[presetKeys[idlePreset]], 0.5);
  isIdle = true;
  isPlaying = false;
}

function goPlaying() {
  isIdle = false;
  isPlaying = true;

  if (currentPreset) {
    visualizer.loadPreset(presets[presetKeys[currentPreset]], 0.5);
  }

  playBufferSource(notificationBuffer);
  updateEffects();

  clearTimeout(voiceTimeout);
  voiceTimeout = setTimeout(function() {
    startVoice();
  }, 1000);
}

export default { start: goPlaying, stop: goIdle };
