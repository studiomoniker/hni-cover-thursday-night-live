import $ from 'jquery';
import isSupported from './isSupported';

let app;
if (isSupported()) {
  const { default: loaded } = require('./main');
  app = loaded;
} else {
  const { default: loaded } = require('./fallback');
  app = loaded;
}

let isPlaying = false;
let intersectionRatio = 0;
const threshold = 0.41;
const onHomepage = /homepage\=1/.test(window.location.search);

const start = () => {
  if (!isPlaying && (intersectionRatio >= threshold || !onHomepage)) {
    app.start();
    isPlaying = true;
  }
};

const stop = () => {
  if (isPlaying && (intersectionRatio >= threshold || !onHomepage)) {
    app.stop();
    isPlaying = false;
  }
};

$('body').mouseenter(start);
$('body').mouseleave(stop);

const observer = new IntersectionObserver(
  entries => {
    entries.forEach(entry => {
      intersectionRatio = entry.intersectionRatio;
      if (intersectionRatio < threshold) {
        stop();
      } else {
        start();
      }
    });
  },
  { threshold: [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0] },
);

observer.observe(document.body);
