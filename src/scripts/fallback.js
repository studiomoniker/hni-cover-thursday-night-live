import $ from 'jquery';

import Typewriter from 'typewriter-effect/dist/core';

var c = null;
var $lineContainer;
var currentLine = 0;
var isAnimating = false;
var isDeleting = false;

var lines = [
  'This browser mutes me.',
  'Please try to open another web environment that supports me.',
  'Thanks, and wish you a lovely Thursday.',
  'Yours truly, Viz',
];

function initBinCanvas() {
  //add new canvas
  'use strict';
  c = document.getElementById('canvas');
  c.classList.add('fallback');
  c.width = window.innerWidth;
  c.height = window.innerHeight;

  window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
  c.width = window.innerWidth;
  c.height = window.innerHeight;
}

let types = [];
function animateText() {
  if (isAnimating || isDeleting || !$lineContainer) return false;
  isAnimating = true;
  changeVisual();
  const line = $lineContainer.children()[currentLine].firstChild;
  const { text } = line.dataset;
  const t = new Typewriter(line)
    .changeDelay(60)
    .typeString(text)
    .start()
    .callFunction(() => {
      setTimeout(() => {
        isAnimating = false;
        currentLine += 1;
        if (currentLine >= lines.length) {
          deleteLines();
        }
      }, 200);
    });
  types.push(t);
}

function deleteLines() {
  types.forEach(t => {
    t.stop();
    t = undefined;
  });
  types = [];

  $('.line').html('');

  currentLine = 0;
  isDeleting = false;
  isAnimating = false;
}

function startCover() {
  animateText();
}

var visuals = [
  'https://media.giphy.com/media/lZlUJYzysCVFu/giphy.gif',
  'https://media.giphy.com/media/l0HlI65K15xz2Iizu/giphy.gif',
  'https://media.giphy.com/media/JQpH25Y6TrRQwtF0KY/giphy.gif',
  'https://media.giphy.com/media/xtGXJX7OyoWbe/giphy.gif',
  'https://media.giphy.com/media/3ohhwoaG5zG7VRHVHq/giphy.gif',
  'https://media.giphy.com/media/hSA7EdFQcjVRFmgiio/giphy.gif',
  'https://media.giphy.com/media/ZxnUA0OpxFjdEr78l2/giphy.gif',
];

function changeVisual() {
  var url = visuals[Math.floor(Math.random() * visuals.length)];
  const img = new Image();
  img.onload = () => {
    document.querySelector('#canvas').style.backgroundImage =
      "url('" + url + "')";
  };
  img.src = url;
}

document.addEventListener('mousemove', startCover);

$(() => {
  'use strict';
  initBinCanvas();
  changeVisual();
  $lineContainer = $('<div class="lines" />');
  $('body').append($lineContainer);

  lines.forEach(l => {
    $lineContainer.append(
      `<div class="line-container"><div class="line" data-text="${l}"></div></div>`,
    );
  });

  animateText();
});

export default {
  start: startCover,
  stop: () => {},
};
